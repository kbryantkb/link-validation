package emailUnitTests;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import utilities.DriverFactory;
import static emailUnitTestsLibs.ImageTestLibs.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;


public class ImageTest {

	//initializes Web Driver and sets browser agent
	private WebDriver driver = DriverFactory.open("chrome");
	
	//get url of email template or web page used to test
	private String webURL = "email url";
	
	//get location of the links doc used to test
	private String userFile = "csv file";
	
	private String fileType = "csv";
	
	@Before
	public void setUp() {
		
		//Web driver navigates to URL used to test
		driver.get(webURL);
		
	}
	
	@Test
	public void imageSRCTest() {
		
		//compares URLs form links doc and email template and tests for equality
		Assert.assertEquals( getImageSRCFromDoc(userFile, fileType), getImageSRCFromHTML(driver));
	}
	
	@Test
	public void imageAltTest() {
		
		//compares URLs form links doc and email template and tests for equality
		Assert.assertEquals( getImageAltFromDoc(userFile, fileType), getImageAltFromHTML(driver));
	}
	
	@After
	public void tearDown() {
		
		//closes browser
		driver.close();
		
	}
}
