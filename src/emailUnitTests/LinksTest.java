package emailUnitTests;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import utilities.DriverFactory;
import static emailUnitTestsLibs.LinksTestLibs.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

public class LinksTest {
	
	//initializes Web Driver and sets browser agent
	private WebDriver driver = DriverFactory.open("chrome");
	
	//get url of email template or web page used to test
	private String webURL = "email url";
	
	//get location of the links doc used to test
	private String userFile = "csv file";
	
	private String fileType = "csv";
	
	@Before
	public void setUp() {
		
		//Web driver navigates to URL used to test
		driver.get(webURL);
		
	}
	
	@Test
	public void urlTest() {
		
		//compares URLs form links doc and email template and tests for equality
		Assert.assertEquals(getURLSFromDoc(userFile, fileType),getURLSFromHTML(driver));
		
	}
	
	@Test
	public void queryStringTest() {
		
		//compare query strings form links doc and emaill template and tests for equality
		Assert.assertEquals(getQueryStringsFromDoc(userFile, fileType), getQueryStringsHTML(driver));
		
	}
	
	@Test
	public void aliasTest() {
		
		//compare query strings form links doc and emaill template and tests for equality
		Assert.assertEquals(getAliasFromDoc(userFile, fileType), getAliasFromHTML(driver));
		
	}
	
	@After
	public void tearDown() {
		
		//closes browser
		driver.close();
		
	}

}