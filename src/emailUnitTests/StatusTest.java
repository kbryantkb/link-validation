package emailUnitTests;

import org.junit.Test;
import static emailUnitTestsLibs.LinkStatusTestLibs.*;
import org.junit.Assert;

public class StatusTest {
	
	private String browser ="chrome";
	private String webURL = "email template";
	
	@Test
	public void linkStatusTest() throws Exception {
		
		//compares URLs form links doc and email template and tests for equality
		//expectedStatus(driver);
		Assert.assertEquals( expectedStatus(browser, webURL), 
				getLinkStatus(statusFeed(browser, webURL)));;
	
	}
	
}
