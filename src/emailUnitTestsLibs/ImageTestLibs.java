package emailUnitTestsLibs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilities.CSV;
import utilities.Excel;

public class ImageTestLibs {
	
	public static ArrayList<String> getImageSRCFromDoc(String userFile, String fileType) {	
		
		//prepares variable for links doc array
		ArrayList <String> imageSRCList = new ArrayList <String>();
		
		//prepares variable for concatenation
		String imageSRCConcat;
		String excel = "excel";
		List <String[]> records;
		
		// creates an array of String arrays from CSV or excel
		if (fileType.equals(excel)) {
			String[][] recordArray = Excel.get(userFile);
			records = Arrays.asList(recordArray);
		} else {
			records = CSV.get(userFile);
		}
		
		// loops through array generated from CSV and adds concatenated QA_ID with link URLs to linksDocList array
		for (String[] record : records) {
			//ignores the first row assuming that the first row is for field titles
			if(records.indexOf(record) >= 1) {
				
				/*
				 * if the URL_ignore column for the record = ignore, concatenate the QA_ID record with ignore
				 * if the URL_ignore column for the record != ignore, concatenate the QA_ID with the URL
				 */
				String imageSRCIgnore = record[5].toLowerCase();
				String recordIgnore = "ignore";
				if(imageSRCIgnore.equals(recordIgnore)) {
					imageSRCConcat = record[0] + "_" + imageSRCIgnore;
				}else {
					imageSRCConcat = record[0] + "_" + record[4];
				}
				imageSRCList.add(imageSRCConcat);
			}
		}
		
		System.out.println("Docs Image SRC" + imageSRCList);
		return imageSRCList;
		
	}
	
public static ArrayList<String> getImageAltFromDoc(String userFile, String fileType) {	
		
		//prepares variable for links doc array
		ArrayList <String> imageAltList = new ArrayList <String>();
		
		//prepares variable for concatenation
		String imageAltConcat;
		String excel = "excel";
		List <String[]> records;
		
		// creates an array of String arrays from CSV or excel
		if (fileType.equals(excel)) {
			String[][] recordArray = Excel.get(userFile);
			records = Arrays.asList(recordArray);
		} else {
			records = CSV.get(userFile);
		}
		
		// loops through array generated from CSV and adds concatenated QA_ID with link URLs to linksDocList array
		for (String[] record : records) {
			//ignores the first row assuming that the first row is for field titles
			if(records.indexOf(record) >= 1) {
				
				/*
				 * if the URL_ignore column for the record = ignore, concatenate the QA_ID record with ignore
				 * if the URL_ignore column for the record != ignore, concatenate the QA_ID with the URL
				 */
				String imageAltIgnore = record[7].toLowerCase();
				String recordIgnore = "ignore";
				if(imageAltIgnore.equals(recordIgnore)) {
					imageAltConcat = record[0] + "_" + imageAltIgnore;
				}else {
					imageAltConcat = record[0] + "_" + record[6];
				}
				imageAltList.add(imageAltConcat);
			}
		}
		
		System.out.println("Docs Image Alt" + imageAltList);
		return imageAltList;
		
	}
	
	public static ArrayList<String> getImageSRCFromHTML(WebDriver driver) {
		
		// prepares variable for array of html link URLs
		ArrayList <String> pageImageSRCList = new ArrayList<String>();
		
		// prepares array to place all of the <a></a> tags found in the HTML
		List <WebElement> aElements = driver.findElements(By.tagName("img"));
		
		// loops through all the <a></a> tags found in the HTML
		for (WebElement aElement : aElements) {
			
			/*
			 * grabs the href attribute value and stores it into a variable
			 * grabs the QA_ID attribute value and stores it in a variable
			 * concatenates the QA_ID value with the href value and stores them in a variable
			 */
			String imageSRCAttr = aElement.getAttribute("src");
			String QA_ID = aElement.getAttribute("QA_IMG_ID");
			String imageSRCConcat = QA_ID + "_" + imageSRCAttr;
			String imageSRCIgnoreAttr = aElement.getAttribute("image_source_ignore");
			String combIgnore = QA_ID + "_" + imageSRCIgnoreAttr;
			String combIgnoreVal = "ignore";
			
			/*
			 * if the QA_ID is not null then add value to pageLinksList
			 * if URL_ignore attribute="ignore" in html, then add combIgnore value to pageLinksList
			 * else add linkConcat to pageLinksList
			 */
			if(!Objects.isNull(QA_ID)) {
				if (Objects.equals(imageSRCIgnoreAttr, combIgnoreVal)) {
					pageImageSRCList.add(combIgnore);
				}else {
					pageImageSRCList.add(imageSRCConcat);
				}
			}
		}
		
		System.out.println("HTML Image SRC" + pageImageSRCList);
		return pageImageSRCList;
		
	}
	
public static ArrayList<String> getImageAltFromHTML(WebDriver driver) {
		
		// prepares variable for array of html link URLs
		ArrayList <String> pageImageAltList = new ArrayList<String>();
		
		// prepares array to place all of the <a></a> tags found in the HTML
		List <WebElement> aElements = driver.findElements(By.tagName("img"));
		
		// loops through all the <a></a> tags found in the HTML
		for (WebElement aElement : aElements) {
			
			/*
			 * grabs the href attribute value and stores it into a variable
			 * grabs the QA_ID attribute value and stores it in a variable
			 * concatenates the QA_ID value with the href value and stores them in a variable
			 */
			String imageAltAttr = aElement.getAttribute("alt");
			String QA_ID = aElement.getAttribute("QA_IMG_ID");
			String imageAltConcat = QA_ID + "_" + imageAltAttr;
			String imageAltIgnoreAttr = aElement.getAttribute("image_alt_ignore");
			String combIgnore = QA_ID + "_" + imageAltIgnoreAttr;
			String combIgnoreVal = "ignore";
			
			/*
			 * if the QA_ID is not null then add value to pageLinksList
			 * if URL_ignore attribute="ignore" in html, then add combIgnore value to pageLinksList
			 * else add linkConcat to pageLinksList
			 */
			if(!Objects.isNull(QA_ID)) {
				if (Objects.equals(imageAltIgnoreAttr, combIgnoreVal)) {
					pageImageAltList.add(combIgnore);
				}else {
					pageImageAltList.add(imageAltConcat);
				}
			}
		}
		
		System.out.println("HTML Image Alt" + pageImageAltList);
		return pageImageAltList;
		
	}

}
