package emailUnitTestsLibs;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilities.DriverFactory;

public class LinkStatusTestLibs {
	
public static ArrayList<String> expectedStatus(String browser, String webURL) {
	
	//initializes Web Driver and sets browser agent
	WebDriver driver = DriverFactory.open(browser);
	
	//Web driver navigates to URL used to test
	driver.get(webURL);
		
		// prepares variable for array of html link URLs
		ArrayList <String> pageStatusList = new ArrayList<String>();
		
		// prepares array to place all of the <a></a> tags found in the HTML
		List <WebElement> aElements = driver.findElements(By.tagName("a"));
		
		// loops through all the <a></a> tags found in the HTML
		for (WebElement aElement : aElements) {
			
			/*
			 * grabs the href attribute value and stores it into a variable
			 * grabs the QA_ID attribute value and stores it in a variable
			 * concatenates the QA_ID value with the href value and stores them in a variable
			 */
			String QA_ID = aElement.getAttribute("QA_ID");
			String statusConcat = QA_ID + "_200";
			String statusIgnoreAttr = aElement.getAttribute("status_ignore");
			String combIgnore = QA_ID + "_" + statusIgnoreAttr;
			String combIgnoreVal = "ignore";
			
			/*
			 * if the QA_ID is not null then add value to pageLinksList
			 * if URL_ignore attribute="ignore" in html, then add combIgnore value to pageLinksList
			 * else add linkConcat to pageLinksList
			 */
			if(!Objects.isNull(QA_ID)) {
				if (Objects.equals(statusIgnoreAttr, combIgnoreVal)) {
					pageStatusList.add(combIgnore);
				}else {
					pageStatusList.add(statusConcat);
				}
			}
		}
		
		//Web driver navigates to URL used to test
		driver.close();
		System.out.println("Link Status Expected" + pageStatusList);
		return pageStatusList;
		
	}

public static ArrayList<String> statusFeed(String browser, String webURL) {
	
	//initializes Web Driver and sets browser agent
	WebDriver driver = DriverFactory.open(browser);
	
	//Web driver navigates to URL used to test
	driver.get(webURL);
	
	// prepares variable for array of html link URLs
	ArrayList <String> pageLinksList = new ArrayList<String>();
	
	// prepares array to place all of the <a></a> tags found in the HTML
	List <WebElement> aElements = driver.findElements(By.tagName("a"));
	
	// loops through all the <a></a> tags found in the HTML
	for (WebElement aElement : aElements) {
		
		/*
		 * grabs the href attribute value and stores it into a variable
		 * grabs the QA_ID attribute value and stores it in a variable
		 * concatenates the QA_ID value with the href value and stores them in a variable
		 */
		String hrefAttr = aElement.getAttribute("href");
		String QA_ID = aElement.getAttribute("QA_ID");
		String linkConcat;
		
		if ( hrefAttr != null && hrefAttr.contains("?")) {
			String[] splitHref = hrefAttr.split("\\?", 2);
			String URL = splitHref[0];
			linkConcat = QA_ID + "_" + URL;
		} else {
			linkConcat = QA_ID + "_" + hrefAttr;
		}
		
		String urlIgnoreAttr = aElement.getAttribute("status_ignore");
		String combIgnore = QA_ID + "_" + urlIgnoreAttr;
		String combIgnoreVal = "ignore";
		
		
		/*
		 * if the QA_ID is not null then add value to pageLinksList
		 * if URL_ignore attribute="ignore" in html, then add combIgnore value to pageLinksList
		 * else add linkConcat to pageLinksList
		 */
		if(!Objects.isNull(QA_ID)) {
			if (Objects.equals(urlIgnoreAttr, combIgnoreVal)) {
				pageLinksList.add(combIgnore);
			}else {
				pageLinksList.add(linkConcat);
			}
		}
	}
	
	driver.close();
	return pageLinksList;
	
}
	
	public static ArrayList<String> getLinkStatus(ArrayList<String> feed) throws Exception{
		ArrayList<String> statusArray = new ArrayList <String>();
		ArrayList<String> linkArray = feed;
		String ignore = "ignore";
		String link;
		int i;
		for(i = 0; i < linkArray.size(); i++) {
			link =linkArray.get(i);
			if(link.contains(ignore)) {
				statusArray.add(link);
			}else {
				String[] splitLink = link.split("_", 2);
				String linkURL = splitLink[1];
				String QA_ID = splitLink[0];
				try {
					URL	url = new URL(linkURL);
					HttpURLConnection connection = (HttpURLConnection)url.openConnection();
					connection.setRequestMethod("GET");
					connection.connect();
					int code = connection.getResponseCode();
					String codeReturn = Integer.toString(code);
					String codeReturnConcat = QA_ID + "_" + codeReturn;
					statusArray.add(codeReturnConcat);
				} catch (Exception e){
					String invalidConcat = QA_ID + "_invalid";
					statusArray.add(invalidConcat);
				}
			}
		}
		
		System.out.println("Link Status Actual" + statusArray);
		return statusArray;
	}

}
