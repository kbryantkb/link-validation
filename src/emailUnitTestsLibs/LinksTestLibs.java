package emailUnitTestsLibs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utilities.CSV;
import utilities.Excel;
//import utilities.DriverFactory;

public class LinksTestLibs {

	public static ArrayList<String> getURLSFromDoc(String userFile, String fileType) {	
		
		//prepares variable for links doc array
		ArrayList <String> linkDocList = new ArrayList <String>();
		
		//prepares variable for concatenation
		String linkConcat;
		String excel = "excel";
		List <String[]> records;
		
		// creates an array of String arrays from CSV or excel
		if (fileType.equals(excel)) {
			String[][] recordArray = Excel.get(userFile);
			records = Arrays.asList(recordArray);
		} else {
			records = CSV.get(userFile);
		}
		
		// loops through array generated from CSV and adds concatenated QA_ID with link URLs to linksDocList array
		for (String[] record : records) {
			//ignores the first row assuming that the first row is for field titles
			if(records.indexOf(record) >= 1) {
				
				/*
				 * if the URL_ignore column for the record = ignore, concatenate the QA_ID record with ignore
				 * if the URL_ignore column for the record != ignore, concatenate the QA_ID with the URL
				 */
				String urlIgnore = record[3].toLowerCase();
				String recordIgnore = "ignore";
				if(urlIgnore.equals(recordIgnore)) {
					linkConcat = record[0] + "_" + urlIgnore;
				}else {
					linkConcat = record[0] + "_" + record[2];
				}
				linkDocList.add(linkConcat);
			}
		}
		
		System.out.println("Docs URLs" + linkDocList);
		return linkDocList;
		
	}
	
	public static ArrayList<String> getQueryStringsFromDoc(String userFile, String fileType) {	
		
		//prepares variable for links doc array
		ArrayList <String> queryStringList = new ArrayList <String>();
		
		//prepares variable for concatenation
		String queryStringConcat;
		String excel = "excel";
		List <String[]> records;
		
		// creates an array of String arrays from CSV or excel
		if (fileType.equals(excel)) {
			String[][] recordArray = Excel.get(userFile);
			records = Arrays.asList(recordArray);
		} else {
			records = CSV.get(userFile);
		}
		
		// loops through array generated from CSV and adds concatenated QA_ID with link URLs to linksDocList array
		for (String[] record : records) {
			//ignores the first row assuming that the first row is for field titles
			if(records.indexOf(record) >= 1) {
				
				/*
				 * if the URL_ignore column for the record = ignore, concatenate the QA_ID record with ignore
				 * if the URL_ignore column for the record != ignore, concatenate the QA_ID with the URL
				 */
				String queryStringIgnore = record[5].toLowerCase();
				String recordIgnore = "ignore";
				if(queryStringIgnore.equals(recordIgnore)) {
					queryStringConcat = record[0] + "_" + queryStringIgnore;
				}else {
					queryStringConcat = record[0] + "_" + record[4];
				}
				queryStringList.add(queryStringConcat);
			}
		}
		
		System.out.println("Docs Query Strings" + queryStringList);
		return queryStringList;
		
	}
	
	public static ArrayList<String> getAliasFromDoc(String userFile, String fileType) {	
		
		//prepares variable for links doc array
		ArrayList <String> aliasList = new ArrayList <String>();
		
		//prepares variable for concatenation
		String aliasConcat;
		String excel = "excel";
		List <String[]> records;
		
		// creates an array of String arrays from CSV or excel
		if (fileType.equals(excel)) {
			String[][] recordArray = Excel.get(userFile);
			records = Arrays.asList(recordArray);
		} else {
			records = CSV.get(userFile);
		}
		
		// loops through array generated from CSV and adds concatenated QA_ID with link URLs to linksDocList array
		for (String[] record : records) {
			//ignores the first row assuming that the first row is for field titles
			if(records.indexOf(record) >= 1) {
				
				/*
				 * if the URL_ignore column for the record = ignore, concatenate the QA_ID record with ignore
				 * if the URL_ignore column for the record != ignore, concatenate the QA_ID with the URL
				 */
				String aliasIgnore = record[8].toLowerCase();
				String recordIgnore = "ignore";
				if(aliasIgnore.equals(recordIgnore)) {
					aliasConcat = record[0] + "_" + aliasIgnore;
				}else {
					aliasConcat = record[0] + "_" + record[7];
				}
				aliasList.add(aliasConcat);
			}
		}
		
		System.out.println("Docs Alias" + aliasList);
		return aliasList;
		
	}
	
	public static ArrayList<String> getURLSFromHTML(WebDriver driver) {
		
		// prepares variable for array of html link URLs
		ArrayList <String> pageLinksList = new ArrayList<String>();
		
		// prepares array to place all of the <a></a> tags found in the HTML
		List <WebElement> aElements = driver.findElements(By.tagName("a"));
		
		// loops through all the <a></a> tags found in the HTML
		for (WebElement aElement : aElements) {
			
			/*
			 * grabs the href attribute value and stores it into a variable
			 * grabs the QA_ID attribute value and stores it in a variable
			 * concatenates the QA_ID value with the href value and stores them in a variable
			 */
			String hrefAttr = aElement.getAttribute("href");
			String QA_ID = aElement.getAttribute("QA_ID");
			String linkConcat;
			
			if ( hrefAttr != null && hrefAttr.contains("?")) {
				String[] splitHref = hrefAttr.split("\\?", 2);
				String URL = splitHref[0];
				linkConcat = QA_ID + "_" + URL;
			} else {
				linkConcat = QA_ID + "_" + hrefAttr;
			}
			
			String urlIgnoreAttr = aElement.getAttribute("URL_ignore");
			String combIgnore = QA_ID + "_" + urlIgnoreAttr;
			String combIgnoreVal = "ignore";
			
			
			/*
			 * if the QA_ID is not null then add value to pageLinksList
			 * if URL_ignore attribute="ignore" in html, then add combIgnore value to pageLinksList
			 * else add linkConcat to pageLinksList
			 */
			if(!Objects.isNull(QA_ID)) {
				if (Objects.equals(urlIgnoreAttr, combIgnoreVal)) {
					pageLinksList.add(combIgnore);
				}else {
					pageLinksList.add(linkConcat);
				}
			}
		}
		
		System.out.println("HTML URLs" + pageLinksList);
		return pageLinksList;
		
	}
	
	public static ArrayList<String> getQueryStringsHTML(WebDriver driver) {
		
		// prepares variable for array of html link URLs
		ArrayList <String> pageQueryStringList = new ArrayList<String>();
		
		// prepares array to place all of the <a></a> tags found in the HTML
		List <WebElement> aElements = driver.findElements(By.tagName("a"));
		
		// loops through all the <a></a> tags found in the HTML
		for (WebElement aElement : aElements) {
			
			/*
			 * grabs the href attribute value and stores it into a variable
			 * grabs the QA_ID attribute value and stores it in a variable
			 * concatenates the QA_ID value with the href value and stores them in a variable
			 */
			String hrefAttr = aElement.getAttribute("href");
			String QA_ID = aElement.getAttribute("QA_ID");
			String queryStringConcat;
			
			if ( hrefAttr != null && hrefAttr.contains("?")) {
				String[] splitHref = hrefAttr.split("\\?", 2);
				String queryString = splitHref[1];
				queryStringConcat = QA_ID + "_" + queryString;
			} else {
				queryStringConcat = QA_ID + " No string";
			}
			
			String queryStringIgnoreAttr = aElement.getAttribute("query_string_ignore");
			String combIgnore = QA_ID + "_" + queryStringIgnoreAttr;
			String combIgnoreVal = "ignore";
			
			
			/*
			 * if the QA_ID is not null then add value to pageLinksList
			 * if URL_ignore attribute="ignore" in html, then add combIgnore value to pageLinksList
			 * else add linkConcat to pageLinksList
			 */
			if(!Objects.isNull(QA_ID)) {
				if (Objects.equals(queryStringIgnoreAttr, combIgnoreVal)) {
					pageQueryStringList.add(combIgnore);
				}else {
					pageQueryStringList.add(queryStringConcat);
				}
			}
		}
		
		System.out.println("HTML Query Strings" + pageQueryStringList);
		return pageQueryStringList;
		
	}
	
	public static ArrayList<String> getAliasFromHTML(WebDriver driver) {
		
		// prepares variable for array of html link URLs
		ArrayList <String> pageAliasList = new ArrayList<String>();
		
		// prepares array to place all of the <a></a> tags found in the HTML
		List <WebElement> aElements = driver.findElements(By.tagName("a"));
		
		// loops through all the <a></a> tags found in the HTML
		for (WebElement aElement : aElements) {
			
			/*
			 * grabs the href attribute value and stores it into a variable
			 * grabs the QA_ID attribute value and stores it in a variable
			 * concatenates the QA_ID value with the href value and stores them in a variable
			 */
			String aliasAttr = aElement.getAttribute("alias");
			String QA_ID = aElement.getAttribute("QA_ID");
			String aliasConcat = QA_ID + "_" + aliasAttr;
			String aliasIgnoreAttr = aElement.getAttribute("alias_ignore");
			String combIgnore = QA_ID + "_" + aliasIgnoreAttr;
			String combIgnoreVal = "ignore";
			
			/*
			 * if the QA_ID is not null then add value to pageLinksList
			 * if URL_ignore attribute="ignore" in html, then add combIgnore value to pageLinksList
			 * else add linkConcat to pageLinksList
			 */
			if(!Objects.isNull(QA_ID)) {
				if (Objects.equals(aliasIgnoreAttr, combIgnoreVal)) {
					pageAliasList.add(combIgnore);
				}else {
					pageAliasList.add(aliasConcat);
				}
			}
		}
		
		System.out.println("HTML Alias" + pageAliasList);
		return pageAliasList;
		
	}
		
}
