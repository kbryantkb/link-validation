package utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.safari.SafariDriver;

public class DriverFactory {
	
	public static WebDriver open(String browserType) {
		
	//Firefox driver
		
	if(browserType.equals("firefox")) {
		System.setProperty("webdriver.firefox.bin", "/Applications/FirefoxCurrent.app/Contents/MacOS/firefox-bin");
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"/geckodriver");
		return new FirefoxDriver();
	}
	
	//Safari driver
	
	else if (browserType.equals("safari")){
		System.setProperty("webdriver.safari.driver", System.getProperty("user.dir")+"/Safari");
		return new SafariDriver();
	}
		
	//Opera driver
	
	else if(browserType.equals("opera")) {
		System.setProperty("webdriver.opera.driver", System.getProperty("user.dir")+"/operadriver");
		return new OperaDriver();
	}
	
	//Chrome driver
	
	else {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/chromedriver");
		return new ChromeDriver();
	}
	
}

}
